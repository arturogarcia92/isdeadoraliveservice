DROP TABLE IF EXISTS `STATICTICS`;

CREATE TABLE `STATICTICS`
(
 `id` varchar(255),
 `timesSuccess` double,
 `timesFail` double,
 `timesUnknown` double,
  PRIMARY KEY (`id`)
);
