DROP TABLE IF EXISTS `CELEBRITIES`;

CREATE TABLE `CELEBRITIES`
(
 `id` varchar(255),
 `fullname` varchar(255),
 `isdead` boolean,
 `birthdate` date,
 `deathdate` date,
 `pictureurl` varchar(255),
 `celebrityinfourl` varchar(255),

  PRIMARY KEY (`id`)
);



