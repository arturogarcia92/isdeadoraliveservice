package com.agarnach.isdeadoralive

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class IsdeadoraliveApplication

fun main(args: Array<String>) {
    runApplication<IsdeadoraliveApplication>(*args)
}
