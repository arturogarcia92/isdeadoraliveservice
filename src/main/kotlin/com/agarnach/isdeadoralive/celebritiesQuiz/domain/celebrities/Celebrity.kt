package com.agarnach.isdeadoralive.celebritiesQuiz.domain.celebrities

import java.time.LocalDate

data class Celebrity(val id: CelebrityId,
                     val fullName: FullName,
                     val isDead: Boolean,
                     val birthDate: LocalDate,
                     val deathDate: LocalDate,
                     val pictureUrl: PictureURL,
                     val celebrityInfoUrl: InfoURL)
