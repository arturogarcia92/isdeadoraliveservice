package com.agarnach.isdeadoralive.celebritiesQuiz.domain.score

import com.agarnach.isdeadoralive.celebritiesQuiz.domain.celebrities.FullName
import java.time.LocalDate

data class Score(val id: Id,
                 val fullName: FullName,
                 val puntuation: Double,
                 val date: LocalDate
)
