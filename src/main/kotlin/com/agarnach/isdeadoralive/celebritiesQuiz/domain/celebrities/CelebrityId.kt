package com.agarnach.isdeadoralive.celebritiesQuiz.domain.celebrities

import java.util.*

data class CelebrityId(val value: String = UUID.randomUUID().toString())
