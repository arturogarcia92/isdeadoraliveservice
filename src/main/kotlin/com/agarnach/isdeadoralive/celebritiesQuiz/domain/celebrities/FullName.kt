package com.agarnach.isdeadoralive.celebritiesQuiz.domain.celebrities

data class FullName(val value: String) {

    init {
        require(validate(value)) { "The value given is not valid" };
    }


    companion object {
        fun validate(value: String) = when {
            value.length > SIZE_LIMIT -> false
            value.isEmpty() -> false
            else -> true
        }

        private const val SIZE_LIMIT = 100
    }
}
