package com.agarnach.isdeadoralive.celebritiesQuiz.domain.celebrities

data class InfoURL(private val _value: URL) {
    val value: String
    get() = _value.value
}
