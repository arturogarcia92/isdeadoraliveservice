package com.agarnach.isdeadoralive.celebritiesQuiz.domain.replies

enum class ValidAnswers {
    DEAD,
    ALIVE,
    UNKNOWN
}
