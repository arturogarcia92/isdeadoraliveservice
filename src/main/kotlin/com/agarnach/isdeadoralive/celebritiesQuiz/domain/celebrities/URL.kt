package com.agarnach.isdeadoralive.celebritiesQuiz.domain.celebrities

import java.net.MalformedURLException
import java.net.URL

data class URL(val value: String){

    init {
        require(validate(value)) { "The value given is not valid" }
    }


    companion object {
        private fun validate(value: String) = when {
            value.isEmpty() -> false
            else -> isURLWellFormed(value)
        }

        private fun isURLWellFormed(url: String): Boolean {
            try {
                URL(url)
            } catch (e: MalformedURLException) {
                return false
            }
            return true
        }
    }



}
