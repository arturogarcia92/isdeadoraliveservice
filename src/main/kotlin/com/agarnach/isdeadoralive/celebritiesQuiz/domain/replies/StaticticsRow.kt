package com.agarnach.isdeadoralive.celebritiesQuiz.domain.replies

import com.agarnach.isdeadoralive.celebritiesQuiz.domain.score.Id

data class StaticticsRow(val id: String,
                         val timesSuccess: Int,
                         val timesFail: Int,
                         val timesUnknown: Int)

