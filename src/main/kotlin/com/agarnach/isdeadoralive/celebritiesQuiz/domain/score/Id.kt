package com.agarnach.isdeadoralive.celebritiesQuiz.domain.score

import java.util.*

data class Id(val value: String = UUID.randomUUID().toString())
