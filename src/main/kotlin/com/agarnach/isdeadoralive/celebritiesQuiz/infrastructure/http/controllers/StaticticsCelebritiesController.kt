package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.controllers

import com.agarnach.isdeadoralive.celebritiesQuiz.application.StaticticsFinder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class StaticticsCelebritiesController
@Autowired constructor(private val staticticsFinder: StaticticsFinder) {



    @GetMapping("/statictics/succes")
    fun retrieveTop10MostSuccessCelebrities() =
            ResponseEntity.ok(staticticsFinder.findTop10success())


    @GetMapping("/statictics/fail")
    fun retrieveTop10MostFailCelebrities() =
            ResponseEntity.ok(staticticsFinder.findTop10Fail())

    @GetMapping("/statictics/unknown")
    fun retrieveTop10MostUnknownCelebrities() =
            ResponseEntity.ok(staticticsFinder.findTop10Unknown())


}
