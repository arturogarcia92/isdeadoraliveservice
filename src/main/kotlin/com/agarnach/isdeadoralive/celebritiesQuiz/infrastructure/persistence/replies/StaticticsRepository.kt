package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.replies

import com.agarnach.isdeadoralive.celebritiesQuiz.domain.replies.StaticticsRow
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.responses.CelebrityStatictic
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.celebrities.CelebrityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class StaticticsRepository
@Autowired constructor(private val staticticsDAO: StaticticsDAO,
                       private val celebrityRepository: CelebrityRepository) {

    fun save(staticticsRow: StaticticsRow) {
        staticticsDAO.save(map(staticticsRow))
    }

    fun find(id: String) = staticticsDAO.findById(id)

    fun findTop10Success(): List<CelebrityStatictic>? = staticticsDAO.findTop10ByOrderByTimessuccessDesc()?.map { statictics -> mapToCelebrityStatistic(statictics, statictics.timessuccess) }

    fun findTop10Fail(): List<CelebrityStatictic>? = staticticsDAO.findTop10ByOrderByTimesfailDesc()?.map { statictics -> mapToCelebrityStatistic(statictics, statictics.timesfail) }

    fun findTop10Unknown(): List<CelebrityStatictic>? = staticticsDAO.findTop10ByOrderByTimesunknownDesc()?.map { statictics -> mapToCelebrityStatistic(statictics, statictics.timesunknown) }


    private fun mapToCelebrityStatistic(statictics: StaticticsRowVO, times: Int): CelebrityStatictic {
        return CelebrityStatictic(id = statictics.id,
                times = times,
                fullName = findCelebrityById(statictics.id).fullname,
                pictureURL = findCelebrityById(statictics.id).pictureurl,
                infoUrl = findCelebrityById(statictics.id).celebrityinfourl
        )
    }

    private fun findCelebrityById(id: String) =
            celebrityRepository.findCelebrityById(id)


    private fun map(staticticsRow: StaticticsRow): StaticticsRowVO =
            StaticticsRowVO(id = staticticsRow.id,
                    timessuccess = staticticsRow.timesSuccess,
                    timesfail = staticticsRow.timesFail,
                    timesunknown = staticticsRow.timesUnknown)


}
