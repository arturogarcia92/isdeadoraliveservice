package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.responses

data class CelebrityResponse(val id: String,
                     val fullName: String,
                     val isDead: Boolean,
                     val birthDate: String,
                     val deathDate: String,
                     val pictureUrl: String,
                     val celebrityInfoUrl: String)
