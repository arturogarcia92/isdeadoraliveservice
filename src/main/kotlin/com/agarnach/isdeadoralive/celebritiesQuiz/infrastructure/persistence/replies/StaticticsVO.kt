package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.replies

import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.score.ScoreVO
import org.springframework.data.repository.CrudRepository
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "STATICTICS")
data class StaticticsRowVO(
        @Id
        val id: String,
        val timessuccess: Int,
        val timesfail: Int,
        val timesunknown: Int)


interface StaticticsDAO: CrudRepository<StaticticsRowVO, String> {
    override fun findAll():List<StaticticsRowVO>
    fun findTop10ByOrderByTimessuccessDesc(): List<StaticticsRowVO>?
    fun findTop10ByOrderByTimesfailDesc(): List<StaticticsRowVO>?
    fun findTop10ByOrderByTimesunknownDesc(): List<StaticticsRowVO>?
}

