package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.controllers

import com.agarnach.isdeadoralive.celebritiesQuiz.application.RandomCelebrityRetriever
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class CelebritiesController
@Autowired constructor(private val randomCelebrityRetriever: RandomCelebrityRetriever) {


    @GetMapping("/celebrities/random")
    fun findRandomCelebrity(): ResponseEntity<*> {
        return ResponseEntity.ok(randomCelebrityRetriever.retrieveCelebrity())
    }


    @GetMapping("/celebrities/is-dead/{id}")
    fun checkIfCelebrityIsDead(@PathVariable("id") id: String) =
        ResponseEntity.ok(randomCelebrityRetriever.checkIfCelebrityIsDead(id))



}
