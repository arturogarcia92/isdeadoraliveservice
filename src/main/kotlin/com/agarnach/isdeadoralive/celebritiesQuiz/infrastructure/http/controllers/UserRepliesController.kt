package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.controllers

import com.agarnach.isdeadoralive.celebritiesQuiz.application.StaticticsAndScoreSaver
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.requests.replies.ReplyCommand
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class UserRepliesController
@Autowired constructor(private val staticticsSaver: StaticticsAndScoreSaver) {



    @PostMapping("/replies/")
    fun saveReplies(@RequestBody request: ReplyCommand) {
        staticticsSaver.saveStaticticsAndScore(request)
    }

}
