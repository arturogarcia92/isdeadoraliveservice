package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.requests.replies

data class ReplyCommand(val userId: String, val userName: String, val replies: List<Reply>)
