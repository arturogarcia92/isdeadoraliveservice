package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.responses

data class CelebrityStatictic(val id: String, val times: Int, val fullName: String, val pictureURL: String, val infoUrl: String)
