package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.controllers

import com.agarnach.isdeadoralive.celebritiesQuiz.application.RandomCelebrityRetriever
import com.agarnach.isdeadoralive.celebritiesQuiz.application.RankingCalculator
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.celebrities.CelebrityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class RankingController
@Autowired constructor(private val rankingCalculator: RankingCalculator) {


    @GetMapping("/ranking/")
    fun getRanking(): ResponseEntity<*> {
        return ResponseEntity.ok(rankingCalculator.findRanking())
    }

    @GetMapping("/ranking/{id}")
    fun getUserPositionInRanking(@PathVariable("id") id: String): ResponseEntity<*> {
        return ResponseEntity.ok(rankingCalculator.findUserPositionInRanking(id))
    }

}
