package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.requests.replies

import com.agarnach.isdeadoralive.celebritiesQuiz.domain.replies.ValidAnswers

data class Reply(val celebrityId: String, val userReply: ValidAnswers, val secondsLeft: Double)

