package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.responses

data class IndividualScore(val fullName: String, val puntuation: Double, val date: String)
