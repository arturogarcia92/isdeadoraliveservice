package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.ranking

import com.agarnach.isdeadoralive.celebritiesQuiz.domain.score.Score
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.responses.IndividualScore
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.responses.RankingScore
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.score.ScoreDAO
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.score.ScoreVO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component


@Component
class RankingRepository
@Autowired constructor(private val scoreDAO: ScoreDAO) {

    fun findRanking(): RankingScore? =
            scoreDAO.findAll()?.sortedBy { score -> score.puntuation }?.reversed()?.map(::map)?.let { RankingScore(it) }


    fun findUserPositionInRanking(id: String): Int? {
        val ranking = scoreDAO.findAll()?.sortedBy { score -> score.puntuation }?.reversed()

        return ranking?.indexOfFirst { user -> user.id == id }
    }

    private fun map(score: ScoreVO): IndividualScore =
            IndividualScore(fullName = score.fullname,
                    puntuation = score.puntuation,
                    date = score.date.toString())

    private fun map(score: Score): ScoreVO =
            ScoreVO(id = score.id.value,
                    fullname = score.fullName.value,
                    puntuation = score.puntuation,
                    date = score.date
            )

}
