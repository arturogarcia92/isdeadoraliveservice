package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.score

import org.springframework.data.repository.CrudRepository
import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "SCORE")
data class ScoreVO(
        @Id
        val id: String,
        val fullname: String,
        val puntuation: Double,
        val date: LocalDate
)


interface ScoreDAO : CrudRepository<ScoreVO, String> {
    override fun findAll(): List<ScoreVO>?
}


