package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.requests

data class ScoreCommand(val playerId: String, val fullName: String, val puntuation: Double, val date: String)
