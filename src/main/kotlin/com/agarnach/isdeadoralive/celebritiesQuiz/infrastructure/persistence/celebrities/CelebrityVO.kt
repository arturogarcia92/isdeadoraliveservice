package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.celebrities

import org.springframework.data.repository.CrudRepository
import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "CELEBRITIES")
data class CelebrityVO(
        @Id
        val id: String,
        val fullname: String,
        val isdead: Boolean,
        val birthdate: LocalDate,
        val deathdate: LocalDate,
        val pictureurl: String,
        val celebrityinfourl: String
)


interface CelebrityDAO: CrudRepository<CelebrityVO, String>{
        override fun findAll():List<CelebrityVO>
}

