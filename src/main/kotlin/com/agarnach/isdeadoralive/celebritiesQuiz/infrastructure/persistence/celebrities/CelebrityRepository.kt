package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.celebrities

import com.agarnach.isdeadoralive.celebritiesQuiz.domain.celebrities.Celebrity
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.responses.CelebrityResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*


@Component
class CelebrityRepository
@Autowired constructor(private val celebrityDAO: CelebrityDAO) {

    fun save(celebrity: Celebrity) {

    }

    //TODO delegar en la db para el random
    fun findRandomCelebrity(): CelebrityResponse? =
            celebrityDAO.findAll().map(::map).let(::getRandomElement)


    fun findCelebrityById(id: String): CelebrityVO =
            celebrityDAO.findById(id).get()


    fun checkIfCelebrityIsDead(id: String) =
        celebrityDAO.findById(id).get().isdead



    private fun getRandomElement(celebrities: List<CelebrityResponse>): CelebrityResponse =
            celebrities[Random().nextInt(celebrities.size)]

    private fun map(celebrity: CelebrityVO): CelebrityResponse =
            CelebrityResponse(id = celebrity.id,
                    fullName = celebrity.fullname,
                    isDead = celebrity.isdead,
                    birthDate = celebrity.birthdate.toString(),
                    deathDate = celebrity.deathdate.toString(),
                    pictureUrl = celebrity.pictureurl,
                    celebrityInfoUrl = celebrity.celebrityinfourl
            )

}
