package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.score

import com.agarnach.isdeadoralive.celebritiesQuiz.domain.score.Score
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class ScoreRepository
@Autowired constructor(private val scoreDAO: ScoreDAO) {

    fun save(score: Score) =
        scoreDAO.save(map(score))


    fun map(score: Score): ScoreVO =
        ScoreVO(id = score.id.value,
                fullname =score.fullName.value,
                puntuation = score.puntuation,
                date = score.date
               )

}
