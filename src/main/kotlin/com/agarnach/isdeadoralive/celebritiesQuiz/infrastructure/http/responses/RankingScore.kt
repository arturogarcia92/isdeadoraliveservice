package com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.responses

data class RankingScore(val individualScoreList: List<IndividualScore>)
