package com.agarnach.isdeadoralive.celebritiesQuiz.application

import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.replies.StaticticsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class StaticticsFinder
@Autowired
constructor(private val staticticsRepository: StaticticsRepository) {

    fun findTop10success() =
        staticticsRepository.findTop10Success()

    fun findTop10Fail() =
            staticticsRepository.findTop10Fail()

    fun findTop10Unknown() =
            staticticsRepository.findTop10Unknown()


}
