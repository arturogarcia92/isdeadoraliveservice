package com.agarnach.isdeadoralive.celebritiesQuiz.application

import com.agarnach.isdeadoralive.celebritiesQuiz.domain.score.Score
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.score.ScoreRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ScoreSaver
@Autowired
constructor(private val scoreRepository: ScoreRepository) {

    fun saveMatchScore(score: Score) = scoreRepository.save(score)

}
