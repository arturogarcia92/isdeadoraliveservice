package com.agarnach.isdeadoralive.celebritiesQuiz.application

import com.agarnach.isdeadoralive.celebritiesQuiz.domain.celebrities.FullName
import com.agarnach.isdeadoralive.celebritiesQuiz.domain.replies.StaticticsRow
import com.agarnach.isdeadoralive.celebritiesQuiz.domain.replies.ValidAnswers
import com.agarnach.isdeadoralive.celebritiesQuiz.domain.score.Id
import com.agarnach.isdeadoralive.celebritiesQuiz.domain.score.Score
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.requests.replies.Reply
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.http.requests.replies.ReplyCommand
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.celebrities.CelebrityRepository
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.replies.StaticticsRepository
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.replies.StaticticsRowVO
import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.score.ScoreRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class StaticticsAndScoreSaver
@Autowired
constructor(private val staticticsRepository: StaticticsRepository,
            private val scoreRepository: ScoreRepository,
            private val celebrityRepository: CelebrityRepository) {

    fun saveStaticticsAndScore(request: ReplyCommand) {
        calculateAndSaveScore(request)
        saveStatictics(request)
    }

    private fun saveStatictics(request: ReplyCommand) {
        request.replies.map { reply -> staticticsRepository.save(mapRepliesToStaticticsRow(reply)) }
    }

    private fun calculateAndSaveScore(request: ReplyCommand) {
        val punctuation = request.replies.map { evaluateReplyScore(isCorrectAnswer(it), it.secondsLeft) }.sum()
        scoreRepository.save(Score(Id(request.userId), FullName(request.userName), punctuation, LocalDate.now()))
    }

    private fun evaluateReplyScore(isCorrect: Boolean, seconds: Double): Double {
        return if (isCorrect) {
            10.00 * (evaluateIfSecondsAreTricked(seconds) / 10)
        } else 0.0
    }

    fun evaluateIfSecondsAreTricked(seconds: Double): Double {
        return if(seconds > 9.8) 0.0
        else seconds
    }

    fun mapRepliesToStaticticsRow(reply: Reply): StaticticsRow {
        val currentStaticticsRowVO = findIfThisStatisticExistsYetOrCreateNew(reply.celebrityId)

        return StaticticsRow(id = reply.celebrityId,
                timesSuccess = increaseSuccess(reply, currentStaticticsRowVO),
                timesFail = increaseFail(reply, currentStaticticsRowVO),
                timesUnknown = increaseUnknown(reply, currentStaticticsRowVO)
        )
    }

    private fun findIfThisStatisticExistsYetOrCreateNew(id: String): StaticticsRowVO =
            staticticsRepository.find(id).orElse(StaticticsRowVO(id = id,
                    timessuccess = 0,
                    timesfail = 0,
                    timesunknown = 0))


    private fun increaseSuccess(reply: Reply, currentStaticticsRowVO: StaticticsRowVO): Int =
            when (isCorrectAnswer(reply)) {
                true -> currentStaticticsRowVO.timessuccess + 1
                else -> currentStaticticsRowVO.timessuccess
            }

    private fun increaseFail(reply: Reply, currentStaticticsRowVO: StaticticsRowVO): Int {
        if (!isCorrectAnswer(reply) && reply.userReply.name != "UNKNOWN") {
            return currentStaticticsRowVO.timesfail + 1
        }
        return currentStaticticsRowVO.timesfail
    }

    private fun increaseUnknown(reply: Reply, currentStaticticsRowVO: StaticticsRowVO): Int =
            when (reply.userReply.name) {
                "UNKNOWN" -> currentStaticticsRowVO.timesunknown + 1
                else -> currentStaticticsRowVO.timesunknown
            }

    private fun isCorrectAnswer(reply: Reply): Boolean {
        val isDead = celebrityRepository.checkIfCelebrityIsDead(reply.celebrityId)
        return evaluateAnswer(isDead, reply.userReply)
    }

    private fun evaluateAnswer(isDead: Boolean, userReply: ValidAnswers): Boolean {
        val correctAnswer: String = when (isDead) {
            true -> "DEAD"
            else -> "ALIVE"
        }
        return correctAnswer == userReply.name
    }


}
