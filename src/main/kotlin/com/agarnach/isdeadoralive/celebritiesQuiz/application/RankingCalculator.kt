package com.agarnach.isdeadoralive.celebritiesQuiz.application

import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.ranking.RankingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class RankingCalculator
@Autowired
constructor(private val rankingRepository: RankingRepository){


    fun findRanking()=
        rankingRepository.findRanking()

    fun findUserPositionInRanking(id: String) = rankingRepository.findUserPositionInRanking(id)?.plus(1)

}
