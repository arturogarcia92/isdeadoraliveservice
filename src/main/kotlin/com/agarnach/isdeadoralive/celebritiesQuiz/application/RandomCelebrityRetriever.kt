package com.agarnach.isdeadoralive.celebritiesQuiz.application

import com.agarnach.isdeadoralive.celebritiesQuiz.infrastructure.persistence.celebrities.CelebrityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class RandomCelebrityRetriever
@Autowired
constructor(private val celebrityRepository: CelebrityRepository) {

    fun retrieveCelebrity() =
        celebrityRepository.findRandomCelebrity()

    fun checkIfCelebrityIsDead(id:String) =
            celebrityRepository.checkIfCelebrityIsDead(id)
}
